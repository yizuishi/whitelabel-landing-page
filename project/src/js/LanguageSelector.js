/**
 * All the possible language settings that must be accounted for
 */
var startingLanguage = "en";

var LanguageSelector = {

    currentLanguageCode: 'en',
    currentLanguageObj: {},
    /**
     * Initialise the language selector
     */
    initialise: function(startingLanguage) {
        // Events for language selector
        LanguageSelector.events.initialise();

        // Starting value
        if (typeof startingLanguage == 'undefined') {
            startingLanguage = LanguageSelector.getStartingLanguage();
        }
        LanguageSelector.loadLanguageJSON(startingLanguage);
        LanguageSelector.setLanguage(startingLanguage);
    },

    getStartingLanguage: function(){
        if(window.location.search.search(/language=[a-z][a-z]/) != -1){
            var langArray = window.location.search.match(/language=[a-z][a-z]/);
            var langCode = langArray[0].replace("language=", '');
            return langCode;
        } else {
            return startingLanguage;
        }
    },

    /**
     * Set language with one of the values from LanguageSettings
     *
     * @param value
     */
    setLanguage: function(langCode) {

        $('.timeline-control-language__item').each(function () {
            if($(this).data('language') === langCode){
                LanguageSelector.currentLanguageCode = $(this).data('language');
                $('.js-language-text').text(' (' + LanguageSelector.currentLanguageCode.toUpperCase() + ')');
                $(this).find('.selected-lang').show();
            } else {
                $(this).find('.selected-lang').hide();
            }
        });
    },

    loadLanguageJSON: function(langCode){
        var langFileUrl = "./language/" + langCode + ".json";
        $.getJSON(langFileUrl,function (json) {
            LanguageSelector.currentLanguageObj = json;
            LanguageSelector.updateLanguage(langCode);
        });
    },

    updateLanguage: function(langCode) {
        //Change the text of every .translate element to current language
        $(".translate").each(function () {
            var translateId = $(this).data('translate');
            if(LanguageSelector.currentLanguageObj.hasOwnProperty(translateId)){
                $(this).text(LanguageSelector.currentLanguageObj[translateId]);
                if($(this).parent().attr('title')){
                    $(this).parent().attr('title',LanguageSelector.currentLanguageObj[translateId]);
                }
            }
        });

        //Changes the source of the side buttons and promos to language specific sources
        LanguageSelector.updateImages();

        //Updates the lang tag at the top of the index.php html tag
        LanguageSelector.updateLangTag(LanguageSelector.currentLanguageCode);

        var videoUrl = document.getElementById('videoPlayerIframe').src;
        /*
         * If iframe src path already contains language=xx then replace the lang code with new code
         * else append the language parameter to iframe src url
         */
        if (videoUrl.search(/language=[a-z][a-z]/) != -1){
            videoUrl = document.getElementById('videoPlayerIframe').src.replace(/language=../,"language=" + langCode );
        } else {
            videoUrl += '&language=' + langCode;
        }
        //Change StateMap to correct language
        VideoPlayerInterface.StateMap = LanguageSelector.currentLanguageObj.StateMap;
        document.getElementById('videoPlayerIframe').src = videoUrl;

        //Change cardMap to correct language
        if(LanguageSelector.currentLanguageObj.CardMap){
            SideButtons.cardMap = LanguageSelector.currentLanguageObj.CardMap;
        }

    },

    updateLangTag: function(newLangCode){
        $('html').attr('lang', newLangCode);
    },

    updateImages: function () {
        var json = LanguageSelector.currentLanguageObj;
        if(json.SideButtonsImagePath && json.SideButtonsImagePathIE){
            $(".side-button").css('background-image', "url(" + json.SideButtonsImagePath + ")");
            $(".ie7 .side-button, .ie8 .side-button").css('background-image', "url(" + json.SideButtonsImagePathIE + ")");
        }

        if(json.PromoTopImagePath && json.PromoTopImagePathIE) {
            $(".promo__top").css('background-image', "url(" + json.PromoTopImagePath + ")");
            $(".ie7 .promo__top, .ie8 .promo__top").css('background-image', "url(" + json.PromoTopImagePathIE + ")");
        }

        if(json.PromoBottomImagePath && json.PromoBottomImagePathIE) {
            $(".promo__bottom").css('background-image', "url(" + json.PromoBottomImagePath + ")");
            $(".ie7 .promo__bottom, .ie8 .promo__bottom").css('background-image', "url(" + json.PromoBottomImagePathIE + ")");
        }
    },

    getTextByKey: function (key) {
        if(typeof LanguageSelector.currentLanguageObj[key] === 'string'){
            return LanguageSelector.currentLanguageObj[key];
        }
        return false;
    },

    /**
     * Define the events
     */
    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            $('#jsLanguageMenuTitle').click(LanguageSelector.events.closeLanguageMenu);
            $('.timeline-control-language__item').click(LanguageSelector.events.languageItemClickEventHandler);
        },

        closeLanguageMenu: function (e) {
            $('#jsSettingsButtonPopout').show();
            $('#jsLanguageSelectorPopout').hide();
        },

        languageItemClickEventHandler: function(e) {
            var newLang = $(this).data('language');
            LanguageSelector.loadLanguageJSON(newLang);
            LanguageSelector.setLanguage(newLang);
            ClosedCaptionSelector.setClosedCaptions('off');
        }
    }
};
